package com.example.spring4.controller;

import com.example.spring4.entity.Book;
import com.example.spring4.repository.BookRepository;
import com.example.spring4.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/books")
    public Iterable<Book> getBooks() {
        return bookService.getBooks();
    }

    @GetMapping("/books/{id}")
    public Optional<Book> getBook(@PathVariable Long id) {
        return bookService.getBook(id);
    }

    @GetMapping("/books/title/{title}")
    // retour erreur : required et provided
    // required = type attendu
    // provided = type fourni
    public Book getByTitle(@PathVariable String title) {
        return bookRepository.getByTitle(title);
    }

    @DeleteMapping("/books/{id}")
    public void deleteBookByID(@PathVariable Long id) {
        bookService.deleteBookByID(id);
    }

    @PostMapping("/books")
    private Book saveBook(@RequestBody Book book) {
        return bookService.save(book);
    }

    @PutMapping("/books/{id}")
    private Book update(@PathVariable Long id, @RequestBody Book book) {
        return bookService.update(id, book);
    }

}
