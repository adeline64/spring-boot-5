package com.example.spring4.controller;

import com.example.spring4.entity.Author;
import com.example.spring4.entity.Book;
import com.example.spring4.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping("/authors")
    public Iterable<Author> getAuthors() {
        return authorService.getAuthors();
    }

    @GetMapping("/authors/{id}")
    public Optional<Author> getAuthor(@PathVariable Long id) {
        return authorService.getAuthor(id);
    }

    @GetMapping("/authors/{authorId}/books")
    public Iterable<Book> getAuthorsBooks(@PathVariable("authorId") Long id) {
        // recupere l'auteur grâce à l'id
        var optionalAuthor = authorService.getAuthor(id);
        // retourner la liste des livres de l'auteur
        if (optionalAuthor.isPresent()) {
            var author = optionalAuthor.get();
            return author.getBookList();
        } else {
            return null;
        }
    }

    @DeleteMapping("/authors/{id}")
    public void deleteAuthorByID(@PathVariable Long id) {
        authorService.deleteAuthorByID(id);
    }

    @PostMapping("/authors")
    private Author saveAuthor(@RequestBody Author author) {
        return authorService.save(author);
    }

    @PutMapping("/authors/{id}")
    private Author update(@PathVariable Long id, @RequestBody Author author) {
        return authorService.update(id, author);
    }

}
