package com.example.spring4.service;

import java.util.Optional;

import com.example.spring4.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring4.entity.Book;
import com.example.spring4.repository.BookRepository;

import lombok.Data;

@Data
@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public Iterable<Book> getBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBook(final Long id) {
        return bookRepository.findById(id);
    }

    public void deleteBookByID(final Long id) {
        bookRepository.deleteById(id);
    }

    public Book save(Book book)
    {
        return bookRepository.save(book);
    }

    public Book update(final Long id, Book book){
        var b = bookRepository.findById(id);
        if (b.isPresent()){
            var courentBook = b.get();
            courentBook.setTitle(book.getTitle());
            courentBook.setDescription(book.getDescription());
            courentBook.setAvailable(book.isAvailable());
            courentBook.setCategory(book.getCategory());
            courentBook.setAuthorList(book.getAuthorList());
            return bookRepository.save(courentBook);
        }else {
            return null;
        }
    }

}
