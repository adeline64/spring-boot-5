package com.example.spring4.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.spring4.entity.Author;
import com.example.spring4.repository.AuthorRepository;

import com.example.spring4.entity.Book;
import com.example.spring4.repository.BookRepository;

import lombok.Data;

@Data
@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public Iterable<Author> getAuthors() {
        return authorRepository.findAll();
    }

    public Optional<Author> getAuthor(final Long id) {
        return authorRepository.findById(id);
    }

    public void deleteAuthorByID(final Long id) {
        authorRepository.deleteById(id);
    }

    public Author save(Author author)
    {
        return authorRepository.save(author);
    }

    public Author update(final Long id, Author author){
        var a = authorRepository.findById(id);
        if (a.isPresent()){
            var courentAuthor = a.get();
            courentAuthor.setNom(author.getNom());
            courentAuthor.setPrenom(author.getPrenom());
            courentAuthor.setBookList(author.getBookList());
            return authorRepository.save(courentAuthor);
        }else {
            return null;
        }
    }


}
