package com.example.spring4.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.spring4.entity.Book;
import com.example.spring4.entity.Category;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {

    Book getByTitle(String title);

}
